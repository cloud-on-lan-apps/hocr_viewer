import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hocr_viewer/hocr_viewer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final ByteData imageData = await rootBundle.load('assets/hindi2.JPG');
  final String hOCRJson = await rootBundle.loadString('assets/hindi2.json');

  runApp(MyApp(imageData: imageData, hOCRJson: hOCRJson));
}

class MyApp extends StatelessWidget {
  final ByteData imageData;
  final String hOCRJson;

  const MyApp({Key? key, required this.imageData, required this.hOCRJson})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HOCR Demo',
      home: HOCRViewer(
        imageAsByteData: imageData,
        hocrAsJSONString: hOCRJson,
      ),
    );
  }
}
