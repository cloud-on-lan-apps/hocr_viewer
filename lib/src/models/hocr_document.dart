import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'ocr_content.dart';

class HOCRDocument {
  final Map<String, dynamic> head;
  final Map<String, List<OCRContent>> body;

  HOCRDocument({required this.head, required this.body});

  HOCRDocument copyWith({
    Map<String, dynamic>? head,
    Map<String, List<OCRContent>>? body,
  }) {
    return HOCRDocument(
      head: head ?? this.head,
      body: body ?? this.body,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'head': head,
      'body': body,
    };
  }

  factory HOCRDocument.fromMap(Map<dynamic, dynamic> map) {
    final Map<String, dynamic> head = {};
    Map<String, List<OCRContent>>? body;
    if (map.containsKey('html')) {
      for (final entry in (map['html'] as Map).entries) {
        if (entry.key == 'body') {
          body = OCRContent.getTags(entry.value, isBody: true);
        } else {
          head[entry.key] = entry.value;
        }
      }
    }
    if (body != null) {
      return HOCRDocument(body: body, head: head);
    }
    throw Exception("Unable to extract HOCR");
  }

  String toJson() => json.encode(toMap());

  factory HOCRDocument.fromJson(String source) =>
      HOCRDocument.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'HOCRDocument(head: $head, body: $body)';

  @override
  bool operator ==(covariant HOCRDocument other) {
    if (identical(this, other)) return true;

    return mapEquals(other.head, head) && mapEquals(other.body, body);
  }

  @override
  int get hashCode => head.hashCode ^ body.hashCode;

  List<OCRContent> getClass(String className) {
    List<OCRContent> items = [];
    for (final entry in body.entries) {
      for (final OCRContent content in entry.value) {
        items.addAll(content.getClass(className));
      }
    }
    return items;
  }

  List<OCRContent> get pageList => getClass('ocr_page');
  List<OCRContent> get careaList => getClass('ocr_carea');
  List<OCRContent> get parList => getClass('ocr_par');
  List<OCRContent> get lineList => getClass('ocr_line');

  static Future<HOCRDocument> fromJSONasync(String source) async {
    return HOCRDocument.fromJson(source);
  }
}
