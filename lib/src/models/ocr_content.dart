import 'dart:convert';
import 'package:flutter/foundation.dart';

import 'ocr_title.dart';

class OCRContent {
  final String hocrClass;
  final String hocrId;
  final OCRTitle hocrTitle;
  final String? text;
  final String? lang;
  final Map<String, List<OCRContent>>? hocrTags;

  OCRContent({
    required this.hocrClass,
    required this.hocrId,
    required this.hocrTitle,
    this.text,
    this.lang,
    this.hocrTags,
  });

  OCRContent copyWith({
    String? hocrClass,
    String? hocrId,
    OCRTitle? hocrTitle,
    String? text,
    String? lang,
    Map<String, List<OCRContent>>? hocrTags,
  }) {
    return OCRContent(
      hocrClass: hocrClass ?? this.hocrClass,
      hocrId: hocrId ?? this.hocrId,
      hocrTitle: hocrTitle ?? this.hocrTitle,
      text: text ?? this.text,
      lang: lang ?? this.lang,
      hocrTags: hocrTags ?? this.hocrTags,
    );
  }

  Map<String, dynamic> toMap({bool unicodeValueForText = true}) {
    return <String, dynamic>{
      "@class": hocrClass,
      "@id": hocrId,
      if (lang != null) '@lang': lang,
      "@title": hocrTitle,
      if (text != null)
        '#text': unicodeValueForText
            ? text!.codeUnits
                .map((e) => e < 128
                    ? String.fromCharCode(e)
                    : '\\u${e.toRadixString(16).padLeft(4, '0')}')
                .join()
            : text,
      if (hocrTags != null)
        for (final entry in hocrTags!.entries)
          entry.key: (entry.value.length == 1)
              ? entry.value[0].toMap()
              : [for (OCRContent item in entry.value) item.toMap()]
    };
  }

  factory OCRContent.fromMap(Map<String, dynamic> map) {
    return OCRContent(
        hocrClass: map['@class'] as String,
        hocrId: map['@id'] as String,
        lang: map['@lang'] as String?,
        hocrTitle: OCRTitle.fromCSV(map['@title'] as String),
        text: map['#text'] as String?,
        hocrTags: getTags(map));
  }

  String toJson() => jsonEncode(toMap());

  factory OCRContent.fromJson(String source) =>
      OCRContent.fromMap(jsonDecode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'OCRContent(hocrClass: $hocrClass, hocrId: $hocrId, hocrTitle: $hocrTitle, text: $text, lang: $lang, hocrTags: $hocrTags)';
  }

  @override
  bool operator ==(covariant OCRContent other) {
    if (identical(this, other)) return true;

    return other.hocrClass == hocrClass &&
        other.hocrId == hocrId &&
        other.hocrTitle == hocrTitle &&
        other.text == text &&
        other.lang == lang &&
        mapEquals(other.hocrTags, hocrTags);
  }

  @override
  int get hashCode {
    return hocrClass.hashCode ^
        hocrId.hashCode ^
        hocrTitle.hashCode ^
        text.hashCode ^
        lang.hashCode ^
        hocrTags.hashCode;
  }

  List<OCRContent> getClass(String className) {
    List<OCRContent> items = [];
    if (hocrClass == className) {
      items.add(this);
    }
    if (hocrTags != null) {
      for (MapEntry tag in hocrTags!.entries) {
        for (OCRContent content in tag.value) {
          items.addAll(content.getClass(className));
        }
      }
    }
    return items;
  }

  static Map<String, List<OCRContent>> getTags(Map<dynamic, dynamic> map,
      {bool isBody = false}) {
    Map<String, List<OCRContent>> hocrTags = {};
    for (final entry in map.entries) {
      if (['div', 'p', 'span'].contains(entry.key)) {
        if (hocrTags[entry.key] == null) {
          hocrTags[entry.key] = [];
        }
        if (entry.value is List) {
          hocrTags[entry.key]!.addAll(
              (entry.value as List).map((e) => OCRContent.fromMap(e)).toList());
        } else {
          hocrTags[entry.key]!.add(OCRContent.fromMap(entry.value));
        }
      } else if ((!isBody) &&
          ['@class', '@id', '@title', '#text', '@lang'].contains(entry.key)) {
        // valid keys for tags except body
      } else {
        throw Exception("Unknown tag ${entry.key}");
      }
    }
    return hocrTags;
  }
}
