import 'dart:convert';
import 'package:flutter/foundation.dart';

class OCRTitle {
  List<double>? bbox;
  Map<String, String>? meta;

  OCRTitle({this.bbox, this.meta});

  OCRTitle copyWith({List<double>? bbox, Map<String, String>? meta}) {
    return OCRTitle(bbox: bbox ?? this.bbox, meta: meta ?? this.meta);
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{'bbox': bbox, 'meta': meta};
  }

  factory OCRTitle.fromMap(Map<String, dynamic> map) {
    return OCRTitle(
      bbox: map['bbox'] != null
          ? List<double>.from((map['bbox'] as List<double>))
          : null,
      meta: map['meta'] != null
          ? Map<String, String>.from((map['meta'] as Map<String, String>))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory OCRTitle.fromJson(String source) =>
      OCRTitle.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'OCRTitle(bbox: $bbox, meta: $meta)';

  @override
  bool operator ==(covariant OCRTitle other) {
    if (identical(this, other)) return true;

    return listEquals(other.bbox, bbox) && mapEquals(other.meta, meta);
  }

  @override
  int get hashCode => bbox.hashCode ^ meta.hashCode;

  factory OCRTitle.fromCSV(String title) {
    List<double>? bbox;
    Map<String, String> meta = {};
    List<String> items = title.split(';');
    for (String item in items) {
      List<String> tmp = item.trim().split(' ');
      String key = tmp[0].trim();
      String values = tmp.sublist(1).join(' ').trim();
      switch (key) {
        case 'bbox':
          bbox = values.split(' ').map((e) => double.parse(e)).toList();
          break;

        default:
          meta[key] = meta.containsKey(key)
              ? meta[key] = "${meta[key]} $values"
              : values;
      }
    }

    return OCRTitle(bbox: bbox, meta: meta.isEmpty ? null : meta);
  }
}
