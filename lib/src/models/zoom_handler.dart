// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:flutter/material.dart';

class ZoomHandler {
  final Size _size;
  final double zoom;

  ZoomHandler({
    required Size size,
    this.zoom = 1.0,
  }) : _size = size;

  ZoomHandler copyWith({
    Size? size,
    double? zoom,
  }) {
    return ZoomHandler(
      size: size ?? _size,
      zoom: zoom ?? this.zoom,
    );
  }

  Size get size => Size(_size.width * zoom, _size.height * zoom);
  double get scale => zoom;

  ZoomHandler zoomFitScreenMultiple(Size screenSize, {double multiple = 1.0}) {
    final double zoomW = (screenSize.width * multiple) / _size.width;
    final double zoomH = (screenSize.height * multiple) / _size.height;

    return copyWith(zoom: zoomW < zoomH ? zoomW : zoomH);
  }
}
