// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:typed_data';

import 'package:flutter/material.dart';

import 'models/hocr_document.dart';
import 'widgets/custom_slider.dart';
import 'widgets/draw_rect.dart';
import 'widgets/positioned_gesture_detector.dart';

class HOCRViewer extends StatefulWidget {
  final ByteData imageAsByteData;
  final String hocrAsJSONString;
  const HOCRViewer({
    Key? key,
    required this.imageAsByteData,
    required this.hocrAsJSONString,
  }) : super(key: key);

  @override
  State<HOCRViewer> createState() {
    return HOCRViewerState();
  }
}

class HOCRViewerState extends State<HOCRViewer> {
  double scale = 1.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('hOCR viewer'),
      ),
      body: Center(child: imageWrapper()),
    );
  }

  Widget imageWrapper() {
    return FutureBuilder(
        future: HOCRDocument.fromJSONasync(widget.hocrAsJSONString),
        builder: ((context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }
          HOCRDocument? doc = snapshot.data as HOCRDocument?;
          if (doc != null) {
            if (doc.pageList.length > 1) {
              throw Exception("Expected only one page");
            }
          }
          final computedScale =
              scale * (MediaQuery.of(context).size.width / 4032);
          return Column(
            children: [
              CustomSlider(
                value: scale,
                onChanged: (value) => setState(() {
                  scale = value;
                }),
                labels: const [0.25, 0.5, 0.75, 1, 1.5, 2.0],
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.all(8),
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            width: 3024 * computedScale,
                            height: 4032 * computedScale,
                            decoration: BoxDecoration(
                              border: Border.all(),
                              color: Colors.black,
                            ),
                            child: Stack(
                              children: [
                                Image.memory(
                                    fit: BoxFit.fill,
                                    widget.imageAsByteData.buffer
                                        .asUint8List()),
                                if (doc != null)
                                  for (final carea in doc.careaList)
                                    if (carea.hocrTitle.bbox != null) ...[
                                      CustomPaint(
                                        painter: (DrawRect.fromLTRB(
                                            strokeWidth: (computedScale * 4)
                                                .roundToDouble(),
                                            color: Colors.amber,
                                            bFill: false,
                                            ltrb: carea.hocrTitle.bbox!
                                                .map((e) => e * computedScale)
                                                .toList())),
                                      ),
                                      PositionedGestureDetector.fromLTRB(
                                        ltrb: carea.hocrTitle.bbox!
                                            .map((e) => e * computedScale)
                                            .toList(),
                                        child: const Center(
                                            child:
                                                Text("This is the text here")),
                                      )
                                    ],
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        }));
  }
}
