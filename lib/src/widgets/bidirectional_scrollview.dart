import 'package:flutter/material.dart';

class BidirectionalScrollView extends StatelessWidget {
  final Size size;
  final List<Widget> children;
  const BidirectionalScrollView({
    Key? key,
    required this.size,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(4),
      child: Stack(
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                width: size.width,
                height: size.height,
                decoration: BoxDecoration(
                  border: Border.all(),
                  color: Colors.black,
                ),
                child: Stack(
                  children: children,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
