import 'package:flutter/material.dart';

class CustomSlider extends StatelessWidget {
  final double value;
  final void Function(double) onChanged;
  final List<double> labels;
  final SliderThemeData? sliderThemeData;

  const CustomSlider({
    Key? key,
    required this.value,
    required this.onChanged,
    required this.labels,
    this.sliderThemeData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: (sliderThemeData != null)
          ? sliderThemeData!
          : SliderTheme.of(context),
      child: Slider(
        value: value,
        min: 0,
        max: labels.length.toDouble() - 1,
        divisions: labels.length,
        label: labels[value.round()].toString(),
        onChanged: onChanged,
      ),
    );
  }
}

/*
.copyWith(
  activeTrackColor: Colors.red[700],
  inactiveTrackColor: Colors.red[100],
  trackShape: const RoundedRectSliderTrackShape(),
  trackHeight: 4.0,
  thumbShape:
      const RoundSliderThumbShape(enabledThumbRadius: 12.0),
  thumbColor: Colors.redAccent,
  overlayColor: Colors.red.withAlpha(32),
  overlayShape:
      const RoundSliderOverlayShape(overlayRadius: 28.0),
  tickMarkShape: const RoundSliderTickMarkShape(),
  activeTickMarkColor: Colors.red[700],
  inactiveTickMarkColor: Colors.red[100],
  valueIndicatorShape: const PaddleSliderValueIndicatorShape(),
  valueIndicatorColor: Colors.redAccent,
  valueIndicatorTextStyle: const TextStyle(
    color: Colors.white,
  ),) 
*/