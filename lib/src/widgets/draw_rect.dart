// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class DrawRect extends CustomPainter {
  final Color color;
  final double strokeWidth;
  final bool bFill;
  final Rect rect;
  DrawRect(
      {required this.rect,
      this.strokeWidth = 4,
      this.bFill = true,
      this.color = const ui.Color.fromARGB(255, 0x8, 0xFF, 0x8)});

  DrawRect.fromLTRB(
      {required List<double> ltrb,
      this.strokeWidth = 4,
      this.bFill = true,
      this.color = const ui.Color.fromARGB(255, 0x8, 0xFF, 0x8)})
      : rect = Rect.fromLTRB(ltrb[0], ltrb[1], ltrb[2], ltrb[3]);

  @override
  void paint(Canvas canvas, Size size) {
    if (bFill) {
      final paint = Paint()
        ..color = color.withAlpha(100)
        ..strokeWidth = 1
        ..strokeCap = StrokeCap.round;
      canvas.drawRect(rect, paint);
    } else {
      final paint = Paint()
        ..color = color
        ..strokeWidth = strokeWidth
        ..strokeCap = StrokeCap.round;

      const pointMode = ui.PointMode.polygon;

      final points = [
        Offset(rect.left, rect.top),
        Offset(rect.left, rect.bottom),
        Offset(rect.right, rect.bottom),
        Offset(rect.right, rect.top),
        Offset(rect.left, rect.top),
      ];
      canvas.drawPoints(pointMode, points, paint);
    }
  }

  @override
  bool shouldRepaint(DrawRect oldDelegate) {
    return (oldDelegate != this);
  }

  @override
  bool operator ==(covariant DrawRect other) {
    if (identical(this, other)) return true;

    return other.color == color &&
        other.strokeWidth == strokeWidth &&
        other.bFill == bFill &&
        other.rect == rect;
  }

  @override
  int get hashCode {
    return color.hashCode ^
        strokeWidth.hashCode ^
        bFill.hashCode ^
        rect.hashCode;
  }
}
