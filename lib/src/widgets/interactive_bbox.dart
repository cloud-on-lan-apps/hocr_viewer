import 'package:flutter/material.dart';

class InteractiveBBox extends StatelessWidget {
  final Rect rect;
  final double iconSize;
  final bool bFill;
  final Decoration boxDecoration;
  final Icon? topRightIcon;
  final void Function()? topRightAction;
  final Widget? child;
  final void Function()? onTap;

  final Rect trIcon;

  InteractiveBBox({
    Key? key,
    required this.rect,
    this.iconSize = 10,
    required this.bFill,
    this.topRightIcon,
    this.topRightAction,
    required this.boxDecoration,
    this.child,
    this.onTap,
  })  : trIcon = Rect.fromLTWH(rect.right - (iconSize / 2),
            rect.top - (iconSize / 2), iconSize, iconSize),
        super(key: key);

  InteractiveBBox.fromLTRB({
    Key? key,
    required List<double> ltrb,
    this.iconSize = 10,
    required this.bFill,
    this.topRightIcon,
    this.topRightAction,
    required this.boxDecoration,
    this.child,
    this.onTap,
  })  : rect = Rect.fromLTRB(ltrb[0], ltrb[1], ltrb[2], ltrb[3]),
        trIcon = Rect.fromLTWH(ltrb[2] - (iconSize / 2),
            ltrb[1] - (iconSize / 2), iconSize, iconSize),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Rect trIcon = Rect.fromLTWH(rect.right - (iconSize / 2),
        rect.top - (iconSize / 2), iconSize, iconSize);
    return Stack(
      children: [
        Positioned.fromRect(
          rect: Rect.fromLTRB(rect.left, rect.top, rect.right, rect.bottom),
          child: GestureDetector(
            onTap: onTap,
            child: DecoratedBox(
              decoration: boxDecoration,
              child:
                  FittedBox(fit: BoxFit.contain, child: child ?? Container()),
            ),
          ),
        ),
        if (topRightAction != null && topRightIcon != null)
          Positioned.fromRect(
            rect: trIcon,
            child: GestureDetector(
              onTap: topRightAction,
              child: FittedBox(
                fit: BoxFit.contain,
                child: topRightIcon ??
                    const Icon(Icons.question_mark, color: Colors.blueAccent),
              ),
            ),
          ),
      ],
    );
  }
}
