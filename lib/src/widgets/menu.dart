import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class Menu extends StatelessWidget {
  final bool topMenu;

  const Menu({
    Key? key,
    required this.menuColor,
    this.topMenu = false,
  }) : super(key: key);

  final Color menuColor;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: topMenu ? Alignment.topCenter : Alignment.bottomCenter,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: Row(
          children: [
            const Spacer(),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    menuColor.withOpacity(0.4),
                    menuColor,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Row(
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(MdiIcons.arrowUpBoldCircleOutline),
                            onPressed: () {},
                          ),
                          IconButton(
                            icon:
                                const Icon(MdiIcons.arrowDownBoldCircleOutline),
                            onPressed: () {},
                          ),
                        ],
                      ),
                      const Text("Menu")
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            icon: const Icon(MdiIcons.minusCircle),
                            onPressed: () {},
                          ),
                          IconButton(
                            icon: const Icon(MdiIcons.fitToScreenOutline),
                            onPressed: () {},
                          ),
                          IconButton(
                            icon: const Icon(MdiIcons.plusCircle),
                            onPressed: () {},
                          )
                        ],
                      ),
                      const Text("Zoom")
                    ],
                  ),
                ],
              ),
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}

/*

*/
/* 

FutureBuilder(
            
            builder: ((context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              }
              HOCRDocument? doc = snapshot.data as HOCRDocument?;
            }))));
children: [
                                Image.memory(
                                    fit: BoxFit.fill,
                                    widget.imageAsByteData.buffer
                                        .asUint8List()),
                              ],

class HVSCrollable extends StatelessWidget {
  final List<Widget> children;
  const HVSCrollable({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
} */

/**
 * 
 * CustomPaint(
          painter: DrawRect(
            rect: rect,
            strokeWidth: strokeWidth,
            color: Colors.amber,
            bFill: bFill,
          ),
        ),
 */

/*
double scale = 1.0;
  final computedScale = scale * (MediaQuery.of(context).size.width / 4032);
CustomSlider(
                value: scale,
                onChanged: (value) => setState(() {
                  scale = value;
                }),
                labels: [0.25, 0.5, 0.75, 1, 1.5, 2.0]
                    .map((e) => e * (MediaQuery.of(context).size.width / 4032))
                    .toList(),
              ),
if(doc == null)
          {
            const Text("HOCR read Failed");
          }

          ZoomHandler(size: )

          if (doc != null) {
            if (doc.pageList.length > 1) {
              throw Exception("Expected only one page");
            }
          }

          return Column(
            children: [
              Expanded(
                child: BidirectionalScrollView(
                    size: Size(imgWidth, imgHeight),
                    children: [
                      Image.memory(
                          fit: BoxFit.fill,
                          widget.imageAsByteData.buffer.asUint8List()),
                    ]),
              ),
            ],
          );

  List<Widget> drawCarea(HOCRDocument? doc) {
    return [
      if (doc != null)
        for (final carea in doc.careaList)
          if (carea.hocrTitle.bbox != null)
            InteractiveBBox.fromLTRB(
              ltrb: carea.hocrTitle.bbox!.map((e) => e * scale).toList(),
              iconSize: (scale * 4).roundToDouble() * 16,
              bFill: false,
              topRightAction: () => print("Clicked ${carea.hocrId}"),
              boxDecoration: BoxDecoration(
                color: const Color.fromARGB(0x1F, 0x08, 0xFF, 0x08),
                border: Border.all(
                    color: const Color.fromARGB(0xFF, 0x08, 0xFF, 0x08)),
              ),
            )
    ];
  }
*/