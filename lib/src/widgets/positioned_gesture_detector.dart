import 'package:flutter/material.dart';

class PositionedGestureDetector extends StatelessWidget {
  final Rect rect;
  final Widget child;
  final void Function()? onTap;

  const PositionedGestureDetector(
      {Key? key, required this.rect, required this.child, this.onTap})
      : super(key: key);

  PositionedGestureDetector.fromLTRB(
      {Key? key, required List<double> ltrb, required this.child, this.onTap})
      : rect = Rect.fromLTRB(ltrb[0], ltrb[1], ltrb[2], ltrb[3]),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned.fromRect(
      rect: Rect.fromLTRB(rect.left, rect.top, rect.right, rect.bottom),
      child: GestureDetector(
        onTap: onTap,
        child: child,
      ),
    );
  }
}
